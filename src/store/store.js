import {createStore, applyMiddleware} from 'redux';
import client from '../reducers';
import rootSaga from '../sagas/client';
import createSagaMiddleware from 'redux-saga'

const sagaMiddleware = createSagaMiddleware();
const store = createStore(client, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);


export default store;
