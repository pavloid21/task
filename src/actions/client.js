import * as types from '../actions/action-types';

export const requestedClients = () => {
  return { type: types.REQUESTED_CLIENTS }
};

export const recievedClients = (data) => {
  return {type: types.RECIEVED_CLIENTS, data}
}
