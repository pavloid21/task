import React, {Component} from 'react'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import {requestedClients} from '../actions/client'

class ClientList extends Component {

	componentDidMount() {
		this.props.requestedClients()
	}

	person = (client, i) => (
			<tr>
				<th scope="row">{client.id}</th>
				<td>{client.name}</td>
				<td>{client.patronymic}</td>
				<td>{client.surname}</td>
				<td>{client.phone}</td>
				<td>{client.email}</td>
				<td>{client.subjects}</td>
				<td>{client.note}</td>
				<td>{client.city}</td>
			</tr>
	)

	render(){
		const {clients =[]} = this.props.data;
		return(
			<div className="container">
				<h2>Client List</h2>
				<table className="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Patronymic</th>
							<th>Surname</th>
							<th>Phone</th>
							<th>Email</th>
							<th>Subjects</th>
							<th>Note</th>
							<th>City</th>
						</tr>
					</thead>
					<tbody>
						{clients.map(this.person)}
					</tbody>
				</table>
				<br/>
				<button className="btn btn-primary">+ Add client</button>
			</div>

			)
	}
}

const mapStateToProps = state => ({data: state.data});
const mapDispatchToProps = dispatch => bindActionCreators({requestedClients}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ClientList)
