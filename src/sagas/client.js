import {recievedClients} from '../actions/client'
import {put, call, takeEvery, takeLatest} from 'redux-saga/effects'
import {fetchData} from './api';

function* fetchClients(action) {
  try {
    const data = yield call(fetchData, )
    yield put(recievedClients(data))
  } catch(e){
    console.log(e);
  }
}

export default function* rootSaga() {
  yield takeLatest('FETCHED_CLIENTS', fetchClients);
}
