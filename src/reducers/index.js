import { combineReducers } from 'redux';

// Reducers
import clientReducer from './client';


// Combine Reducers
var reducers = combineReducers({
    clientState: clientReducer
});

export default reducers;
