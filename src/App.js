import React, { Component } from 'react';
import Nav from './components/Nav';
import {Provider} from 'react-redux';
import store from './store/store';
import ClientList from './components/ClientList';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <Nav/>
          <ClientList/>
        </div>
      </Provider>
    );
  }
}

export default App;
